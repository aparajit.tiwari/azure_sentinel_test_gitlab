output "resource_group_name" {
  value = azurerm_resource_group.example.name
}

output "log_analytics_workspace_name" {
  value = azurerm_log_analytics_workspace.example.name
}

# output "log_analytics_workspace_primary_shared_key" {
#   value = azurerm_log_analytics_workspace.example.primary_shared_key
# }

# output "log_analytics_workspace_secondary_shared_key" {
#   value = azurerm_log_analytics_workspace.example.secondary_shared_key
# }

output "sentinel_alert_rule_name" {
  value = azurerm_sentinel_alert_rule_ms_security_incident.example.name
}

output "sentinel_alert_rule_product_filter" {
  value = azurerm_sentinel_alert_rule_ms_security_incident.example.product_filter
}

output "sentinel_alert_rule_display_name" {
  value = azurerm_sentinel_alert_rule_ms_security_incident.example.display_name
}

output "sentinel_alert_rule_severity_filter" {
  value = azurerm_sentinel_alert_rule_ms_security_incident.example.severity_filter
}
/////////////////////ARS/////////////////////
output "alert_rule_id" {
  description = "The ID of the Sentinel alert rule."
  value       = azurerm_sentinel_alert_rule_scheduled.example.id
}
