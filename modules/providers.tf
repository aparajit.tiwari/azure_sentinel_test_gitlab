terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.54.0"
    }
  }
}

provider "azurerm" {
  features {}

  #subscription_id = "14f6258c-60d9-483f-aef5-6138df8f0f57"
}
