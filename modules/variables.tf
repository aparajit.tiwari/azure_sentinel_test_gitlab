variable "location" {
  type        = string
  description = "The location/region where the resources will be created."
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group."
}

variable "workspace_name" {
  type        = string
  description = "The name of the workspace."
}

variable "sku" {
  type        = string
  description = "The SKU of the workspace."
}

variable "product_filter" {
  type        = string
  description = "The product filter for the Sentinel alert rule."
}

variable "display_name" {
  type        = string
  description = "The display name for the Sentinel alert rule."
}

variable "severity_filter" {
  type        = list(string)
  description = "The severity filter for the Sentinel alert rule."
}

variable "alert_rule_name" {
  type        = string
  description = "The name for the Sentinel alert rule."
}

variable "data_connector_name" {
  description = "The name of the Sentinel data connector."
  type        = string
}

////////////////////

variable "display_name2" {
  description = "The display name for the Sentinel alert rule."
  type        = string
}

variable "severity" {
  description = "The severity level for the Sentinel alert rule."
  type        = string
}

variable "query" {
  description = "The KQL query for the Sentinel alert rule."
  type        = string
}
