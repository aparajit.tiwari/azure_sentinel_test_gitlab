resource "azurerm_resource_group" "example" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_log_analytics_workspace" "example" {
  name                = var.workspace_name
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  sku                 = var.sku
}

resource "azurerm_sentinel_log_analytics_workspace_onboarding" "example" {
  workspace_id = azurerm_log_analytics_workspace.example.id
}

resource "azurerm_sentinel_alert_rule_ms_security_incident" "example" {
  name                       = var.alert_rule_name
  log_analytics_workspace_id = azurerm_sentinel_log_analytics_workspace_onboarding.example.workspace_id
  product_filter             = var.product_filter
  display_name               = var.display_name
  severity_filter            = var.severity_filter
}

//##########################AzureActiveDirectory##########################//
resource "azurerm_sentinel_data_connector_azure_active_directory" "example" {
  name                       = var.data_connector_name
  log_analytics_workspace_id = azurerm_sentinel_log_analytics_workspace_onboarding.example.workspace_id
}

//##########################SentinelAlertRuleSchedule##########################//
resource "azurerm_sentinel_alert_rule_scheduled" "example" {
  name                       = var.display_name2
  log_analytics_workspace_id = azurerm_sentinel_log_analytics_workspace_onboarding.example.workspace_id
  display_name               = var.display_name2
  severity                   = var.severity
  query                      = var.query
}

//////////////////////////////////////////////////
# resource "azurerm_resource_group" "example" {
#   name     = var.resource_group_name
#   location = var.location
# }

# resource "azurerm_log_analytics_workspace" "example" {
#   name                = var.log_analytics_workspace_name
#   location            = var.location
#   resource_group_name = azurerm_resource_group.example.name
#   sku                 = var.sku
# }


# resource "azurerm_sentinel_alert_rule_scheduled" "example" {
#   name                       = "example"
#   log_analytics_workspace_id = azurerm_sentinel_log_analytics_workspace_onboarding.example.workspace_id
#   display_name               = "example"
#   severity                   = "High"
#   query                      = var.alert_rule_query
# }
