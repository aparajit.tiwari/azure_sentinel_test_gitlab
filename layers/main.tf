module "sentinel" {
  source = "../modules"


  location = var.location
  resource_group_name = var.resource_group_name
  workspace_name = var.workspace_name
  sku = var.sku
  product_filter = var.product_filter
  display_name = var.display_name
  severity_filter = var.severity_filter
  alert_rule_name = var.alert_rule_name
  ///AAD
  data_connector_name = var.data_connector_name

  ///ARS
  display_name2 = var.display_name2
  severity = var.severity
  query = var.query
}

