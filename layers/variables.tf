variable "location" {
  description = "The location/region where the resources will be created."
  default     = "West Europe"
}

variable "resource_group_name" {
  description = "The name of the resource group."
  default     = "example-resources"
}

variable "workspace_name" {
  description = "The name of the workspace."
  default     = "example-workspace"
}

variable "sku" {
  description = "The SKU of the workspace."
  default     = "PerGB2018"
}

variable "product_filter" {
  description = "The product filter for the Sentinel alert rule."
  default     = "Microsoft Cloud App Security"
}

variable "display_name" {
  description = "The display name for the Sentinel alert rule."
  default     = "example rule"
}

variable "severity_filter" {
  description = "The severity filter for the Sentinel alert rule."
  default     = ["High"]
}

variable "alert_rule_name" {
  description = "The name for the Sentinel alert rule."
  default     = "example-ms-security-incident-alert-rule"
}

variable "data_connector_name" {
  description = "The name of the Sentinel data connector."
  default     = "example"
}

//////////////////////////

variable "display_name2" {
  description = "The display name for the Sentinel alert rule."
  default     = "example"
}

variable "severity" {
  description = "The severity level for the Sentinel alert rule."
  default     = "High"
}

variable "query" {
  description = "The KQL query for the Sentinel alert rule."
  default     = <<QUERY
AzureActivity |
  where OperationName == "Create or Update Virtual Machine" or OperationName =="Create Deployment" |
  where ActivityStatus == "Succeeded" |
  make-series dcount(ResourceId) default=0 on EventSubmissionTimestamp in range(ago(7d), now(), 1d) by Caller
QUERY
}
